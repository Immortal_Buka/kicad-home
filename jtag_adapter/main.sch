EESchema Schematic File Version 4
LIBS:main-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x10_Odd_Even J1
U 1 1 5D83373C
P 7425 3100
F 0 "J1" H 7475 3717 50  0000 C CNN
F 1 "Conn_02x10_Odd_Even" H 7475 3626 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x10_P2.54mm_Vertical" H 7425 3100 50  0001 C CNN
F 3 "~" H 7425 3100 50  0001 C CNN
	1    7425 3100
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5D834A33
P 6200 3125
F 0 "J2" H 6250 3542 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 6250 3451 50  0000 C CNN
F 2 "footprints:FTSH-105" H 6200 3125 50  0001 C CNN
F 3 "~" H 6200 3125 50  0001 C CNN
	1    6200 3125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5D835216
P 7825 3850
F 0 "#PWR01" H 7825 3600 50  0001 C CNN
F 1 "GND" H 7830 3677 50  0000 C CNN
F 2 "" H 7825 3850 50  0001 C CNN
F 3 "" H 7825 3850 50  0001 C CNN
	1    7825 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7725 3600 7825 3600
Wire Wire Line
	7825 3600 7825 3750
Wire Wire Line
	7725 3500 7825 3500
Wire Wire Line
	7825 3500 7825 3600
Connection ~ 7825 3600
Wire Wire Line
	7725 3400 7825 3400
Wire Wire Line
	7825 3400 7825 3500
Connection ~ 7825 3500
Wire Wire Line
	7725 3300 7825 3300
Wire Wire Line
	7825 3300 7825 3400
Connection ~ 7825 3400
Wire Wire Line
	7725 3200 7825 3200
Wire Wire Line
	7825 3200 7825 3300
Connection ~ 7825 3300
Wire Wire Line
	7725 3100 7825 3100
Wire Wire Line
	7825 3100 7825 3200
Connection ~ 7825 3200
Wire Wire Line
	7725 2800 7825 2800
Wire Wire Line
	7825 2800 7825 2900
Connection ~ 7825 3100
Wire Wire Line
	7725 3000 7825 3000
Connection ~ 7825 3000
Wire Wire Line
	7825 3000 7825 3100
Wire Wire Line
	7725 2900 7825 2900
Connection ~ 7825 2900
Wire Wire Line
	7825 2900 7825 3000
Wire Wire Line
	6000 2925 5850 2925
Wire Wire Line
	5850 2700 7225 2700
Wire Wire Line
	5850 2700 5850 2925
Wire Wire Line
	6500 2925 6875 2925
Wire Wire Line
	6875 2925 6875 3000
Wire Wire Line
	6875 3000 7225 3000
Wire Wire Line
	6500 3025 6850 3025
Wire Wire Line
	6850 3025 6850 3100
Wire Wire Line
	6850 3100 7225 3100
Wire Wire Line
	6500 3125 6825 3125
Wire Wire Line
	6825 3125 6825 3300
Wire Wire Line
	6825 3300 7225 3300
Wire Wire Line
	6000 3025 5850 3025
Wire Wire Line
	5850 3025 5850 3125
Wire Wire Line
	5850 3125 6000 3125
Wire Wire Line
	5850 3125 5850 3325
Wire Wire Line
	5850 3325 6000 3325
Connection ~ 5850 3125
Wire Wire Line
	7825 3750 5850 3750
Wire Wire Line
	5850 3750 5850 3325
Connection ~ 7825 3750
Wire Wire Line
	7825 3750 7825 3850
Connection ~ 5850 3325
Wire Wire Line
	6500 3325 6800 3325
Wire Wire Line
	6800 3325 6800 3400
Wire Wire Line
	6800 3400 7225 3400
Wire Wire Line
	6000 3225 5925 3225
Wire Wire Line
	5925 3225 5925 3500
Wire Wire Line
	5925 3500 6850 3500
Wire Wire Line
	6850 3500 6850 3200
Wire Wire Line
	6850 3200 7225 3200
Wire Wire Line
	7225 2900 6800 2900
Wire Wire Line
	6800 2900 6800 3225
Wire Wire Line
	6800 3225 6500 3225
$EndSCHEMATC
