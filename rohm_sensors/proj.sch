EESchema Schematic File Version 4
LIBS:power
LIBS:device
LIBS:74xx
LIBS:audio
LIBS:interface
LIBS:proj-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	9025 1025 9025 1475
Wire Wire Line
	9025 1475 9125 1475
Wire Wire Line
	10525 1575 10425 1575
Wire Wire Line
	10525 925  10525 1025
Wire Wire Line
	8825 1025 9025 1025
Wire Wire Line
	8825 1075 8825 1025
Connection ~ 9025 1025
Wire Wire Line
	8675 1575 8825 1575
Wire Wire Line
	8825 1375 8825 1575
Text GLabel 8675 1575 0    60   Input ~ 0
INT1
Connection ~ 8825 1575
Text GLabel 4150 1425 0    60   Input ~ 0
INT1
Wire Wire Line
	6175 1575 6125 1575
Wire Wire Line
	6125 1575 6125 1675
Wire Wire Line
	6125 1675 6175 1675
Wire Wire Line
	5825 2025 6125 2025
Wire Wire Line
	6825 2025 6825 1975
Connection ~ 6125 1675
Text GLabel 10575 1475 2    60   Input ~ 0
SCL
Text GLabel 10575 1675 2    60   Input ~ 0
SDA
Text GLabel 4800 1425 2    60   Input ~ 0
INT2
Text GLabel 7575 1775 3    60   Input ~ 0
SCL
Wire Wire Line
	7475 1675 7575 1675
Wire Wire Line
	7575 1675 7575 1775
Text GLabel 7725 1775 3    60   Input ~ 0
SDA
Wire Wire Line
	7475 1575 7725 1575
Wire Wire Line
	7725 1575 7725 1775
Wire Wire Line
	5825 1475 6125 1475
Wire Wire Line
	7475 1475 7575 1475
Wire Wire Line
	7575 1475 7575 1375
Wire Wire Line
	7575 1025 7575 1075
Wire Wire Line
	6125 1025 7575 1025
Connection ~ 6125 1025
Text GLabel 7875 1775 3    60   Input ~ 0
INT2
Wire Wire Line
	7875 1475 7875 1775
Connection ~ 7575 1475
Wire Wire Line
	5825 1575 5825 1475
Connection ~ 6125 1475
Wire Wire Line
	5825 1875 5825 2025
Connection ~ 6125 2025
Wire Wire Line
	8825 1675 9125 1675
Wire Wire Line
	4100 1125 4100 1225
Wire Wire Line
	4100 1225 4200 1225
Text GLabel 4150 1325 0    60   Input ~ 0
SCL
Text GLabel 4800 1325 2    60   Input ~ 0
SDA
Text GLabel 4800 1525 2    60   Input ~ 0
SDA
Text GLabel 4800 1625 2    60   Input ~ 0
SCL
Connection ~ 10525 1025
Wire Wire Line
	10425 1475 10575 1475
Wire Wire Line
	10425 1675 10575 1675
Wire Wire Line
	9025 1025 10525 1025
Wire Wire Line
	8825 1575 9125 1575
Wire Wire Line
	6125 1675 6125 2025
Wire Wire Line
	6125 1025 6125 1475
Wire Wire Line
	7575 1475 7875 1475
Wire Wire Line
	6125 1475 6175 1475
Wire Wire Line
	6125 2025 6825 2025
Wire Wire Line
	10525 1025 10525 1575
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J1
U 1 1 5C9379E0
P 4400 1425
F 0 "J1" H 4450 1842 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 4450 1751 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 4400 1425 50  0001 C CNN
F 3 "~" H 4400 1425 50  0001 C CNN
	1    4400 1425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1325 4200 1325
Wire Wire Line
	4700 1325 4800 1325
Wire Wire Line
	4700 1225 4750 1225
Wire Wire Line
	4750 1225 4750 1725
Wire Wire Line
	4150 1425 4200 1425
Wire Wire Line
	4700 1425 4800 1425
Wire Wire Line
	6825 2125 6825 2025
Connection ~ 6825 2025
Wire Wire Line
	8825 1825 8825 1675
$Comp
L power:+3V3 #PWR01
U 1 1 5C9A3350
P 4100 1125
F 0 "#PWR01" H 4100 975 50  0001 C CNN
F 1 "+3V3" H 4115 1298 50  0000 C CNN
F 2 "" H 4100 1125 50  0001 C CNN
F 3 "" H 4100 1125 50  0001 C CNN
	1    4100 1125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5C9A7C56
P 6825 2125
F 0 "#PWR04" H 6825 1875 50  0001 C CNN
F 1 "GND" H 6830 1952 50  0000 C CNN
F 2 "" H 6825 2125 50  0001 C CNN
F 3 "" H 6825 2125 50  0001 C CNN
	1    6825 2125
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5C9AF324
P 4750 2000
F 0 "#PWR02" H 4750 1750 50  0001 C CNN
F 1 "GND" H 4755 1827 50  0000 C CNN
F 2 "" H 4750 2000 50  0001 C CNN
F 3 "" H 4750 2000 50  0001 C CNN
	1    4750 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR06
U 1 1 5C9B84E2
P 10525 925
F 0 "#PWR06" H 10525 775 50  0001 C CNN
F 1 "+3V3" H 10540 1098 50  0000 C CNN
F 2 "" H 10525 925 50  0001 C CNN
F 3 "" H 10525 925 50  0001 C CNN
	1    10525 925 
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5C9BB5C6
P 6125 950
F 0 "#PWR03" H 6125 800 50  0001 C CNN
F 1 "+3V3" H 6140 1123 50  0000 C CNN
F 2 "" H 6125 950 50  0001 C CNN
F 3 "" H 6125 950 50  0001 C CNN
	1    6125 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C9C5FDE
P 5925 1200
F 0 "R3" V 6025 1225 50  0000 C CNN
F 1 "R" V 5925 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5855 1200 50  0001 C CNN
F 3 "~" H 5925 1200 50  0001 C CNN
	1    5925 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5C9C6057
P 3750 1350
F 0 "R2" V 3850 1350 50  0000 C CNN
F 1 "R" V 3750 1350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 1350 50  0001 C CNN
F 3 "~" H 3750 1350 50  0001 C CNN
	1    3750 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C9C60EC
P 3550 1350
F 0 "R1" V 3650 1350 50  0000 C CNN
F 1 "R" V 3550 1350 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3480 1350 50  0001 C CNN
F 3 "~" H 3550 1350 50  0001 C CNN
	1    3550 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 1525 4800 1525
Wire Wire Line
	4700 1625 4800 1625
Wire Wire Line
	3750 1525 3750 1500
Wire Wire Line
	3750 1525 4200 1525
Wire Wire Line
	3550 1500 3550 1625
Wire Wire Line
	3550 1625 4200 1625
Wire Wire Line
	3750 1200 3750 1125
Wire Wire Line
	3750 1125 4100 1125
Connection ~ 4100 1125
Wire Wire Line
	3550 1200 3550 1125
Wire Wire Line
	3550 1125 3750 1125
Connection ~ 3750 1125
Wire Wire Line
	4700 1725 4750 1725
Connection ~ 4750 1725
Wire Wire Line
	4750 1725 4750 2000
Text GLabel 4150 1725 0    50   Input ~ 0
ADDR
Wire Wire Line
	4150 1725 4200 1725
Text GLabel 5725 1375 0    50   Input ~ 0
ADDR
$Comp
L Device:C C1
U 1 1 5C9F3753
P 5825 1725
F 0 "C1" H 5940 1771 50  0000 L CNN
F 1 "C" H 5940 1680 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 5863 1575 50  0001 C CNN
F 3 "~" H 5825 1725 50  0001 C CNN
	1    5825 1725
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5C9F3805
P 7575 1225
F 0 "R4" H 7645 1271 50  0000 L CNN
F 1 "R" H 7645 1180 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7505 1225 50  0001 C CNN
F 3 "~" H 7575 1225 50  0001 C CNN
	1    7575 1225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C9FA0D3
P 8825 1225
F 0 "R5" H 8895 1271 50  0000 L CNN
F 1 "R" H 8895 1180 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8755 1225 50  0001 C CNN
F 3 "~" H 8825 1225 50  0001 C CNN
	1    8825 1225
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5C9FD52A
P 8825 1825
F 0 "#PWR05" H 8825 1575 50  0001 C CNN
F 1 "GND" H 8830 1652 50  0000 C CNN
F 2 "" H 8825 1825 50  0001 C CNN
F 3 "" H 8825 1825 50  0001 C CNN
	1    8825 1825
	1    0    0    -1  
$EndComp
$Comp
L schematic:BH1730 U2
U 1 1 5C9FEF39
P 9775 1575
F 0 "U2" H 9775 1940 50  0000 C CNN
F 1 "BH1730" H 9775 1849 50  0000 C CNN
F 2 "footprints:rohm_wsof6" H 9775 1575 50  0001 C CNN
F 3 "DOCUMENTATION" H 9775 1575 50  0001 C CNN
	1    9775 1575
	1    0    0    -1  
$EndComp
$Comp
L schematic:BH1745 U1
U 1 1 5CA03DC7
P 6825 1525
F 0 "U1" H 6825 1940 50  0000 C CNN
F 1 "BH1745" H 6825 1849 50  0000 C CNN
F 2 "footprints:rohm_wson008x2120" H 6825 1525 50  0001 C CNN
F 3 "DOCUMENTATION" H 6825 1525 50  0001 C CNN
	1    6825 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5925 1350 5925 1375
Connection ~ 5925 1375
Wire Wire Line
	5925 1375 6175 1375
Wire Wire Line
	5925 1050 5925 1025
Wire Wire Line
	5925 1025 6125 1025
Wire Wire Line
	5725 1375 5925 1375
Wire Wire Line
	6125 950  6125 1025
$EndSCHEMATC
