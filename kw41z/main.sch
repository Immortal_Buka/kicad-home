EESchema Schematic File Version 4
LIBS:main-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J?
U 1 1 5D9F3781
P 10175 1500
F 0 "J?" H 10225 1917 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 10225 1826 50  0000 C CNN
F 2 "" H 10175 1500 50  0001 C CNN
F 3 "~" H 10175 1500 50  0001 C CNN
	1    10175 1500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5DBA4C2D
P 4000 5725
F 0 "#PWR?" H 4000 5475 50  0001 C CNN
F 1 "GND" H 4005 5552 50  0000 C CNN
F 2 "" H 4000 5725 50  0001 C CNN
F 3 "" H 4000 5725 50  0001 C CNN
	1    4000 5725
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 5725 4000 5575
Text GLabel 3000 3600 0    50   Input ~ 0
nRES
Wire Wire Line
	3000 3600 3100 3600
Text GLabel 10575 1700 2    50   Input ~ 0
nRES
Wire Wire Line
	10475 1700 10575 1700
Text GLabel 10575 1400 2    50   Input ~ 0
SWCLK
Text GLabel 10575 1300 2    50   Input ~ 0
SWDIO
Wire Wire Line
	10475 1300 10575 1300
Wire Wire Line
	10575 1400 10475 1400
Wire Wire Line
	9975 1400 9875 1400
Wire Wire Line
	9875 1400 9875 1500
Wire Wire Line
	9875 1500 9975 1500
$Comp
L power:GND #PWR?
U 1 1 5DBADBC2
P 9875 1800
F 0 "#PWR?" H 9875 1550 50  0001 C CNN
F 1 "GND" H 9880 1627 50  0000 C CNN
F 2 "" H 9875 1800 50  0001 C CNN
F 3 "" H 9875 1800 50  0001 C CNN
	1    9875 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9875 1800 9875 1500
Connection ~ 9875 1500
$Comp
L power:VDD #PWR?
U 1 1 5DBAF022
P 9875 1125
F 0 "#PWR?" H 9875 975 50  0001 C CNN
F 1 "VDD" H 9892 1298 50  0000 C CNN
F 2 "" H 9875 1125 50  0001 C CNN
F 3 "" H 9875 1125 50  0001 C CNN
	1    9875 1125
	1    0    0    -1  
$EndComp
Wire Wire Line
	9875 1125 9875 1300
Wire Wire Line
	9875 1300 9975 1300
Text GLabel 3000 3400 0    50   Input ~ 0
SWDIO
Wire Wire Line
	3000 3400 3100 3400
Text GLabel 3000 3500 0    50   Input ~ 0
SWCLK
Wire Wire Line
	3000 3500 3100 3500
Text GLabel 3000 3700 0    50   Input ~ 0
PTA16
Text GLabel 3000 3900 0    50   Input ~ 0
PTA18
Wire Wire Line
	3000 3700 3100 3700
Text GLabel 3000 3800 0    50   Input ~ 0
PTA17
Wire Wire Line
	3000 3800 3100 3800
Text GLabel 3000 4000 0    50   Input ~ 0
PTA19
Text GLabel 1225 1700 0    50   Input ~ 0
PTA16
Text GLabel 1225 1900 0    50   Input ~ 0
PTA18
Text GLabel 1225 1800 0    50   Input ~ 0
PTA17
Text GLabel 1225 2000 0    50   Input ~ 0
PTA19
Wire Wire Line
	3000 3900 3100 3900
Wire Wire Line
	3000 4000 3100 4000
Wire Wire Line
	3100 4100 2825 4100
Wire Wire Line
	2825 4100 2825 5575
Wire Wire Line
	2825 5575 4000 5575
Connection ~ 4000 5575
Wire Wire Line
	4000 5575 4000 5400
$Comp
L schematic:MKW41Z512VHT4 U?
U 1 1 5D9F3FDF
P 4550 3950
F 0 "U?" H 4500 3975 50  0000 L CNN
F 1 "MKW41Z512VHT4" H 4250 3875 50  0000 L CNN
F 2 "MODULE" H 4550 3950 50  0001 C CNN
F 3 "DOCUMENTATION" H 4550 3950 50  0001 C CNN
	1    4550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6075 3500 6075 3800
Wire Wire Line
	6000 3500 6075 3500
Wire Wire Line
	6075 3800 6000 3800
Connection ~ 6075 3500
Wire Wire Line
	6075 3400 6075 3500
Wire Wire Line
	6000 3400 6075 3400
Wire Wire Line
	6175 4400 6000 4400
$Comp
L Device:Antenna_Shield AE?
U 1 1 5DBA2321
P 7325 3375
F 0 "AE?" H 7469 3414 50  0000 L CNN
F 1 "Antenna_Shield" H 7469 3323 50  0000 L CNN
F 2 "" H 7325 3475 50  0001 C CNN
F 3 "~" H 7325 3475 50  0001 C CNN
	1    7325 3375
	1    0    0    -1  
$EndComp
Wire Wire Line
	6875 3700 7000 3700
$Comp
L Device:C_Small C?
U 1 1 5DBA13B7
P 7100 3700
F 0 "C?" V 7329 3700 50  0000 C CNN
F 1 "C_Small" V 7238 3700 50  0000 C CNN
F 2 "" H 7100 3700 50  0001 C CNN
F 3 "~" H 7100 3700 50  0001 C CNN
	1    7100 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:L_Small L?
U 1 1 5DB9FFBF
P 6775 3700
F 0 "L?" V 6960 3700 50  0000 C CNN
F 1 "L_Small" V 6869 3700 50  0000 C CNN
F 2 "" H 6775 3700 50  0001 C CNN
F 3 "~" H 6775 3700 50  0001 C CNN
	1    6775 3700
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5DB9D2A0
P 6525 3875
F 0 "C?" H 6433 3829 50  0000 R CNN
F 1 "C_Small" H 6433 3920 50  0000 R CNN
F 2 "" H 6525 3875 50  0001 C CNN
F 3 "~" H 6525 3875 50  0001 C CNN
	1    6525 3875
	-1   0    0    1   
$EndComp
Wire Wire Line
	6175 3600 6175 4100
Wire Wire Line
	6175 3600 6000 3600
Wire Wire Line
	4000 5575 6175 5575
Wire Wire Line
	6175 5575 6175 4500
Connection ~ 6175 4400
Wire Wire Line
	6000 3700 6525 3700
Wire Wire Line
	6175 4100 6525 4100
Wire Wire Line
	6525 4100 6525 3975
Connection ~ 6175 4100
Wire Wire Line
	6175 4100 6175 4400
Wire Wire Line
	6525 3775 6525 3700
Connection ~ 6525 3700
Wire Wire Line
	6525 3700 6675 3700
Wire Wire Line
	7200 3700 7325 3700
Wire Wire Line
	7325 3700 7325 3575
$Comp
L Device:C_Small C?
U 1 1 5DBE2703
P 7325 3900
F 0 "C?" H 7233 3854 50  0000 R CNN
F 1 "C_Small" H 7233 3945 50  0000 R CNN
F 2 "" H 7325 3900 50  0001 C CNN
F 3 "~" H 7325 3900 50  0001 C CNN
	1    7325 3900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7325 3800 7325 3700
Connection ~ 7325 3700
Wire Wire Line
	6525 4100 6750 4100
Wire Wire Line
	7325 4100 7325 4000
Connection ~ 6525 4100
Wire Wire Line
	7425 3575 7425 4100
Wire Wire Line
	7425 4100 7325 4100
Connection ~ 7325 4100
$Comp
L Device:Crystal_GND24_Small Y?
U 1 1 5DBE6B68
P 6400 4500
F 0 "Y?" H 6544 4546 50  0000 L CNN
F 1 "Crystal_GND24_Small" H 6544 4455 50  0000 L CNN
F 2 "" H 6400 4500 50  0001 C CNN
F 3 "~" H 6400 4500 50  0001 C CNN
	1    6400 4500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 3900 6400 3900
Wire Wire Line
	6400 3900 6400 4400
Wire Wire Line
	6400 4600 6250 4600
Wire Wire Line
	6250 4600 6250 4000
Wire Wire Line
	6250 4000 6000 4000
Wire Wire Line
	6500 4500 6750 4500
Wire Wire Line
	6750 4500 6750 4100
Connection ~ 6750 4100
Wire Wire Line
	6750 4100 7325 4100
Wire Wire Line
	6300 4500 6175 4500
Connection ~ 6175 4500
Wire Wire Line
	6175 4500 6175 4400
$Comp
L power:+3V3 #PWR?
U 1 1 5DBECD8E
P 4400 2175
F 0 "#PWR?" H 4400 2025 50  0001 C CNN
F 1 "+3V3" H 4415 2348 50  0000 C CNN
F 2 "" H 4400 2175 50  0001 C CNN
F 3 "" H 4400 2175 50  0001 C CNN
	1    4400 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 2500 4400 2250
Wire Wire Line
	4400 2250 6075 2250
Wire Wire Line
	6075 2250 6075 3400
Connection ~ 4400 2250
Wire Wire Line
	4400 2250 4400 2175
Connection ~ 6075 3400
Wire Wire Line
	6000 4200 6075 4200
Wire Wire Line
	6075 4200 6075 3800
Connection ~ 6075 3800
Wire Wire Line
	6000 4300 6075 4300
Wire Wire Line
	6075 4300 6075 4200
Connection ~ 6075 4200
Wire Wire Line
	4700 5400 4700 5500
Wire Wire Line
	4700 5500 6075 5500
Wire Wire Line
	6075 5500 6075 4300
Connection ~ 6075 4300
Wire Wire Line
	4200 5500 4700 5500
Connection ~ 4700 5500
Wire Wire Line
	4200 5400 4200 5500
Wire Wire Line
	4100 5400 4100 5500
Wire Wire Line
	4100 5500 4200 5500
Connection ~ 4200 5500
Wire Wire Line
	3100 4200 3050 4200
Wire Wire Line
	3050 4200 3050 2250
Wire Wire Line
	3050 2250 4400 2250
Wire Wire Line
	3050 4200 3050 4300
Wire Wire Line
	3050 4300 3100 4300
Connection ~ 3050 4200
$EndSCHEMATC
